About
-----

This modules enables you to easily duplicate existing fields to other bundles of the same entity type. 

Example use cases:

Node: You need to create serveral content types which share some equal fields.

Commerce: You need to create several product types, which all have the same fields in common. 



Installation
------------

Install this module like any other drupal modules. For more instructions: http://drupal.org/documentation/install/modules-themes/modules-7

Usage (after installation)
--------------------------

1. Goto: admin/structure/attach-existing-fields or via the admin 'structure' > 'Existing fields'

2. Select which entity type you would like to use and continue.

3. You will see a matrix with all enabled fields and bundles. Fill in a label and select to which bundle you want to duplicate the existing field.


